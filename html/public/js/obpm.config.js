/**
 * 5.0
 * **/
let obpmConfig = {
  contextPath: "/obpm/api",
  statiContextPath: "/static",
  obpmFilePath: "/obpm",
  signonContextPath: "/signon",
  messagePath: "/message/api",
  converterPath:"/converter/api",
  kmsContextPath:"/kms/api",
  kmsFilePath:"/kms",
  magicHtmlPath:"/obpm/html",
  magicAppName:'crm',//定制页面的软件名，以后vue写的放这里,html写的有个obpm.config.js，（项目管理、积分商城、培训管理老项目都是脚本自己传的）
  appId: '__dBmwJZB993phCTn0Dcy', //appId
}