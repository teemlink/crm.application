module.exports = {
  css: {
    // 是否使用css分离插件 ExtractTextPlugin
    extract: false,
    // 开启 CSS source maps?
    sourceMap: true,
    // css预设器配置项
    loaderOptions: {
      postcss:{
        plugins:[
        ]
      }
    },
    // 启用 CSS modules for all css / pre-processor files.
    modules: false,
  },
  runtimeCompiler: true,
  productionSourceMap: false,

  publicPath: "./",

  devServer: {
    open: true,
    host: "localhost",
    port: 8085,
    https: false,
    //以上的ip和端口是我们本机的;下面为需要跨域的
    proxy: {//配置跨域
        'obpm': {
          target: 'http://192.168.88.102:8080/',
          ws: true,          
          changOrigin: true,
          pathRewrite: {  
              '^/obpm': '/obpm'
          }
        },
        'static': {
          target: 'http://192.168.88.102:8080/',
          ws: true,          
          changOrigin: true, 
          pathRewrite: {  
              '^/static': '/static'
          }
        },
        'authtime': {
            target: 'http://192.168.88.102:8080/',// ' 这里后台的地址模拟的;应该填写你们真实的后台接口http://192.168.88.191:8080/  http://192.168.10.140/ http://192.168.88.199:8083/
            ws: true,
            changOrigin: true,//允许跨域
            pathRewrite: {
                '^/authtime': '/obpm/authtime'//请求的时候使用这个api就可以
            }
        },
        'runtime': {
            target: 'http://192.168.88.102:8080/',// 这里后台的地址模拟的;应该填写你们真实的后台接口http://192.168.88.191:8080/  http://192.168.10.140/ http://192.168.88.199:8083/
            ws: true,
            changOrigin: true,//允许跨域
            pathRewrite: {
                '^/runtime': '/obpm/runtime'//请求的时候使用这个api就可以
            }
        },
        'rest': {
            target: 'http://192.168.88.102:8080/',//这里后台的地址模拟的;应该填写你们真实的后台接口http://192.168.88.191:8080/  http://192.168.10.140/ http://192.168.88.199:8083/
            ws: true,
            changOrigin: true,//允许跨域
            pathRewrite: {
                '^/rest': '/obpm/rest'//请求的时候使用这个api就可以
            }
        }
    }
  },
  pwa: {
    iconPaths: {
      favicon32: "favicon.ico",
      favicon16: "favicon.ico",
      appleTouchIcon: "favicon.ico",
      maskIcon: "favicon.ico",
      msTileImage: "favicon.ico",
    },
  },
};
