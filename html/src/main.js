import Vue from 'vue'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import App from './App.vue'
import router from './router'
import store from './store'

Vue.config.productionTip = false
import API from '@/api.js'
Vue.prototype.$api = API;
import './assets/style/_reset.scss'
//引入font-awesome
import 'font-awesome/css/font-awesome.css';
//引入iconfont
import './assets/css/iconfont/iconfont.css'

import i18n from './i18n'

Vue.use(ElementUI);
//Vue.use(QRCode);

new Vue({
  router,
  store,
  i18n,
  data: function () {
    return {
      adminToken: ''
    }
  },
  render: h => h(App)
}).$mount('#app')
