import Vue from 'vue'
import Router from 'vue-router'
Vue.use(Router)


const router = new Router({
  mode: 'hash',
  routes: [
    // {
    //   path: '/',
    //   redirect: '/login',
    // },
    // crm管理路由
    {
      path: '/customerInfomation',
      name: 'customerInfomation',
      component: () => import('./components/customerInfomation.vue')
    },
    {
      path: '/test',
      name: 'test',
      component: () => import('./components/test.vue')
    },
    {
      path: '/followRecords',
      name: 'followRecords',
      component: () => import('./components/followRecords.vue')
    },
    {
      path: '/visitorPlan',
      name: 'visitorPlan',
      component: () => import('./components/visitorPlan.vue')
    },
    {
      path: '/widgetStage',
      name: 'widgetStage',
      component: () => import('./components/widgetStage.vue')
    },
    // widgetStage
    
    // {
    //   path: '/card',
    //   name: 'card',
    //   component: () => import('./components/card.vue')
    // },
    // {
    //   path: '/mapview',
    //   name: 'mapview',
    //   component: () => import('./components/view_mapview.vue')
    // },
    
  ]
});
// router.beforeEach((to, from, next) => {
//   next()
// })
export default router;